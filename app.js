let express = require('express');
let app = express();
let path = require('path');

let server = app.listen(3000, function () {
    console.log('Express server listening on port ' + server.address().port);
});

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/admin.html'));
});

module.exports = app;
