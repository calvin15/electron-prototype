// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
let server = require('./app.js')
const { ipcRenderer } = require('electron')
const internalIp = require('internal-ip')
const QRCode = require('qrcode-svg')

let state = false

let qrcode = new QRCode(`http://${internalIp.v4.sync()}:3000`).svg()

window.qrcodewrapper.innerHTML = qrcode
window.iptext.innerHTML = internalIp.v4.sync()
window.statetext.innerHTML = state

server.get('/setFullScreen', function(req, res) {
    if (state) {
        state = !true
    } else {
        state = true
    }
    window.statetext.innerHTML = state
    ipcRenderer.send('setFullScreen', state)

    res.send(state)
});
